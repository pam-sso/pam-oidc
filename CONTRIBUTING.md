# How to contribute to the project

## Git rules

Anyone can open a merge request to this project.
You just need to fork the project and push your changes.
We (the PAM SSO team) will try to review your changes as quickly as possible.

All commits must be signed and must match the following format:

    ^(ci|docs|feat|fix|refactor|test|chore)(\(.*?\))?: .*

The branches must be named like this:

- `feat/*`: a new feature / implementation of more flows
- `fix/*`: a bugfix

## Setup your dev environment

### Build and link the pam module

First compile the project:

    cargo build --release

You may need to install some dependencies (`librust-pam-dev` on debian).

Then add the generated lib to your pam modules directory.

    sudo ln -s `pwd`/target/release/libpam_oidc.so /lib/x86_64-linux-gnu/security/pam_oidc.so

### Create an application that uses the module

To test this module without modifying a real PAM module, we can use the `test.c` file.
First compile it:

    gcc -o test.run test.c -lpam -lpam_misc

Add a pam config file for this application in the `/etc/pam.d/oidc-test` file.

    echo "auth required pam_oidc.so $(pwd)/pam-oidc.json" | sudo tee /etc/pam.d/oidc-test

### Start an OIDC server

You now need to start an OIDC server, here I propose a docker compose setup but any other setup should work.

    docker compose up -d

If this is the first time you run this command, you will need to seed the database :

    bash ./seed.sh

### Configure the module

Add the openid-configuration to the `openid-configuration.json` file.

    sudo curl -Ls -o "/etc/pam-oidc/openid-configuration.json" http://localhost:8080/auth/realms/test/.well-known/openid-configuration

Then the `pam-oidc.json` file should look like this (with a valid OIDC client id and secret):

    sudo cat << EOF > /etc/pam-oidc/config.json
    {
        "flow": "password",
        "client_id": "TODO",
        "client_secret": "TODO",
        "oidc_file": "/etc/pam-oidc/openid-configuration.json"
    }
    EOF

## Test

Now you can run `./test.run john` to test the module.
If you make any changes to the rust code, you just need to recompile the module.

    cargo build --release && ./test.run john
