#!/bin/bash

# end to end tests

# build the module
cargo build --release >/dev/null 2>&1
gcc -o test.run test.c -lpam -lpam_misc

echo user1 | RUST_BACKTRACE=full ./test.run invalid 2>/dev/null | diff -u - expected/auth_err
echo user1 | RUST_BACKTRACE=full ./test.run user1 2>/dev/null | diff -u - expected/auth_ok_account_ok
echo user2 | RUST_BACKTRACE=full ./test.run user2 2>/dev/null | diff -u - expected/auth_ok_account_err
