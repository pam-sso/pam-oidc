use pam::constants::{PamFlag, PamResultCode};
use pam::module::{PamHandle, PamHooks};
use pam::pam_hooks;

mod helpers;
mod oidc;
use helpers::prepare_pam_env;

struct PamOidc;
pam_hooks!(PamOidc);

impl PamHooks for PamOidc {
    fn sm_authenticate(
        pamh: &PamHandle,
        _args: Vec<&std::ffi::CStr>,
        _flags: PamFlag,
    ) -> PamResultCode {
        let env = prepare_pam_env(pamh);
        if env.is_none() {
            return PamResultCode::PAM_AUTH_ERR;
        }
        let (conv, oidc) = env.unwrap(); 
        if !oidc.auth(pamh, conv) {
            return PamResultCode::PAM_AUTH_ERR;
        }
        PamResultCode::PAM_SUCCESS
    }

    fn acct_mgmt(
        pamh: &PamHandle,
        _args: Vec<&std::ffi::CStr>,
        _flags: PamFlag,
    ) -> PamResultCode {
        let env = prepare_pam_env(pamh);
        if env.is_none() {
            return PamResultCode::PAM_AUTH_ERR;
        }
        let (conv, oidc) = env.unwrap(); 
        if !oidc.account(pamh, conv) {
            return PamResultCode::PAM_AUTH_ERR;
        }
        PamResultCode::PAM_SUCCESS
    }
}
