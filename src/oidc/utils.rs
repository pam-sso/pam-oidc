use chrono::Utc;
use reqwest::{blocking::Client, Url};
use serde_json::{from_str as json_from_str, json, Value};
use std::fs;
use std::{collections::HashMap, str::FromStr};
use serde_derive::Deserialize;

#[derive(Deserialize, std::fmt::Debug)]
pub struct DeviceResponse {
    pub device_code: String,
    pub user_code: String,
    pub verification_uri: String,
    pub verification_uri_complete: String,
    pub expires_in: i64,
    pub interval: i64,
}


pub fn get_endpoint(file: &str, endpoint: &str) -> Result<String, Box<dyn std::error::Error>> {
    let res = fs::read_to_string(file).expect("Couldn't read config file");
    let json: Value = json_from_str(&res)?;
    let value = json
        .get(endpoint.to_owned() + "_endpoint")
        .unwrap()
        .as_str()
        .unwrap();
    Ok(value.to_string())
}

fn query_param(hashmap: HashMap<&str, &str>) -> Result<String, Box<dyn std::error::Error>> {
    let mut url = Url::parse("http://osef.com/")?;
    for (key, value) in hashmap {
        url.query_pairs_mut().append_pair(key, value);
    }
    Ok(String::from_str(url.query().unwrap())?)
}

fn login_query_param(
    user: &str,
    pass: &str,
    client_id: &str,
    client_secret: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    query_param(
        vec![
            ("grant_type", "password"),
            ("username", user),
            ("password", pass),
            ("client_id", client_id),
            ("client_secret", client_secret),
        ]
        .into_iter()
        .collect(),
    )
}

fn device_query_param(
    client_id: &str,
    client_secret: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    query_param(
        vec![
            ("grant_type", "password"),
            ("client_id", client_id),
            ("client_secret", client_secret),
        ]
        .into_iter()
        .collect(),
    )
}

fn device_check_param(
    client_id: &str,
    client_secret: &str,
    device_code: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    query_param(
        vec![
            ("grant_type", "urn:ietf:params:oauth:grant-type:device_code"),
            ("client_id", client_id),
            ("client_secret", client_secret),
            ("device_code", device_code),
        ]
        .into_iter()
        .collect(),
    )
}

pub fn send_login_to_endpoint(
    endpoint: &str,
    user: &str,
    pass: &str,
    client_id: &str,
    client_secret: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    let client = Client::new();
    let res = client
        .post(endpoint)
        .header("content-type", "application/x-www-form-urlencoded")
        .header("accept", "application/json")
        .body(login_query_param(user, pass, client_id, client_secret)?)
        .send()?;
    let txt = res.text()?;
    log::debug!("HTTP response : {:?}", txt);
    Ok(txt)
}

pub fn get_device_endpoint(
    endpoint: &str,
    client_id: &str,
    client_secret: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    let client = Client::new();
    let res = client
        .post(endpoint)
        .header("content-type", "application/x-www-form-urlencoded")
        .header("accept", "application/json")
        .body(device_query_param(client_id, client_secret)?)
        .send()?;
    let txt = res.text()?;
    log::debug!("HTTP response : {:?}", txt);
    Ok(txt)
}

pub fn check_device_endpoint(
    endpoint: &str,
    client_id: &str,
    client_secret: &str,
    device_code: &str,
) -> Result<String, Box<dyn std::error::Error>> {
    let client = Client::new();
    let res = client
        .post(endpoint)
        .header("content-type", "application/x-www-form-urlencoded")
        .header("accept", "application/json")
        .body(device_check_param(client_id, client_secret, device_code)?)
        .send()?;
    let txt = res.text()?;
    log::debug!("HTTP response : {:?}", txt);
    Ok(txt)
}

pub fn get_access_token(server_response: &str) -> Result<String, Box<dyn std::error::Error>> {
    let json_req = json_from_str(server_response);
    if json_req.is_err() {
        return Err("Couldn't parse json".into());
    }
    let json: Value = json_req.unwrap();
    let temp_value = json!("");
    let value = json
        .get("access_token")
        .unwrap_or(&temp_value)
        .as_str()
        .unwrap_or("")
        .to_string();
    match value {
        x if x.is_empty() => Err("No access token found".into()),
        _ => Ok(value),
    }
}

fn base64_to_str(input: String) -> String {
    let decoded = base64::decode(input).unwrap();
    String::from_utf8(decoded).unwrap()
}

fn current_time_in_range(start: i64, end: i64) -> bool {
    let now = Utc::now().timestamp();
    start <= now && now <= end
}

fn open_bearer(token: &str) -> (Value, Value, String) {
    let mut it = token.split('.');
    let header = json_from_str(&base64_to_str(it.next().unwrap().to_string())).unwrap();
    let payload = json_from_str(&base64_to_str(it.next().unwrap().to_string())).unwrap();
    let signature = it.next().unwrap().to_string();
    (header, payload, signature)
}

pub fn token_time_is_valid(token: &str) -> Option<bool> {
    let payload = open_bearer(token).1;

    let exp = payload.get("exp")?.as_i64()?;
    let ait = payload.get("iat")?.as_i64()?;
    Some(current_time_in_range(ait, exp))
}

pub fn user_can_use_pam_oidc(token: &str, client: &str) -> Option<bool> {
    // If the user has the PAM group,
    // they can use this pam module to authenticate.
    // lookup if PAM is in payload.resource_access.account.roles
    let payload = open_bearer(token).1;
    let realm_access_roles = payload
        .get("realm_access")?
        .get("roles")?
        .as_array()?
        .iter()
        .map(|x| x.as_str().unwrap_or("").to_string())
        .any(|val| val == "PAM");
    if realm_access_roles {
        return Some(true);
    }
    let resource_access_roles = payload
        .get("resource_access")?
        .get(client)?
        .get("roles")?
        .as_array()?
        .iter()
        .map(|x| x.as_str().unwrap_or("").to_string())
        .any(|val| val == "PAM");
    if resource_access_roles {
        return Some(true);
    }
    Some(false)
}
