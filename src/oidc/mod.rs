mod utils;
use log::error;
use pam::{constants::PAM_PROMPT_ECHO_OFF, conv::PamConv, module::PamHandle};
use utils::{
    get_access_token, get_endpoint, send_login_to_endpoint, token_time_is_valid,
    user_can_use_pam_oidc,get_device_endpoint, DeviceResponse, check_device_endpoint
};

use crate::helpers::config::{Config, Flow, get_config};
use crate::helpers::qrcode::generate_qrcode;

pub struct Oidc {
    config: Config,
}


impl Oidc {
    pub fn new() -> Option<Oidc> {
        let conf = get_config();
        if conf.is_none() {
            error!("Unable to read config file");
            return None;
        }
        Some(Oidc { config: conf.unwrap() })
    }

    pub fn auth(&self, pamh: &PamHandle, conv: &PamConv) -> bool {
        let token = match self.config.flow {
            Flow::Password => self.auth_password(pamh, conv),
            Flow::Device => self.auth_device(pamh, conv),
        };
        if token.is_none() {
            return false;
        }
        let storage = pamh.set_data::<String>("token", Box::new(token.unwrap()));
        if storage.is_err() {
            return false;
        }
        true
    }

    fn auth_password(&self, pamh: &PamHandle, conv: &PamConv) -> Option<String> {
        let user = pamh.get_user(None);
        if user.is_err() {
            return None;
        }
        let user = user.unwrap();
        let message = std::format!("Password for {}: ", user);
        let password = conv.send(PAM_PROMPT_ECHO_OFF, &message);
        if password.is_err() {
            return None;
        }
        let password = password.unwrap();
        if password.is_none() {
            return None;
        }
        let password = password.unwrap();
        let login = self.login(&user, &password);
        if login.is_err() {
            return None;
        }
        Some(login.unwrap())
    }

    fn auth_device(&self, _pamh: &PamHandle, conv: &PamConv) -> Option<String> {
        let endpoint = self.get_endpoint("device_authorization");
        if endpoint.is_err() {
            log::error!("Malformed oidc file : can't find device_authorization_endpoint in {}", self.config.oidc_file);
            return None;
        }
        let endpoint_string = endpoint.unwrap();
        let http_response = get_device_endpoint(
            &endpoint_string,
            &self.config.client_id,
            &self.config.client_secret,
        );
        if http_response.is_err() {
            log::error!("{} returned error : {:?}", endpoint_string, http_response);
            return None;
        }
        let response_body = http_response.unwrap();
        let deserialized: Result<DeviceResponse, serde_json::Error> = serde_json::from_str(&response_body);
        if deserialized.is_err() {
            log::error!("Unable to deserialize data into device response, maybe wrong endpoint : {:?}", response_body);
            return None;
        }
        let deserialized = deserialized.unwrap();

        let url = deserialized.verification_uri_complete;
        if conv.send(pam::constants::PAM_TEXT_INFO, &format!("{}\n{}", url, generate_qrcode(&url))).is_err() {
            log::error!("Unable to comunicate with PAM conv");
            return None;
        }
        let mut t = 0;
        let interval = deserialized.interval as u64;

        loop {
            std::thread::sleep(std::time::Duration::from_secs(interval));
            t += interval;
            if t > deserialized.expires_in as u64 {
                log::info!("Device flow expired");
                return None;
            }

            let check = check_device_endpoint(
                &self.get_endpoint("token").unwrap(),
                &self.config.client_id,
                &self.config.client_secret,
                &deserialized.device_code,
            );
            if !check.is_err() {
                if let Ok(access_token) = get_access_token(&check.unwrap()) {
                    return Some(access_token);
                }
            }
        };
    }

    pub fn account(&self, pamh: &PamHandle, _conv: &PamConv) -> bool {
        let jwt;
        unsafe { jwt = pamh.get_data::<String>("token") }
        if jwt.is_err() {
            return false;
        }
        let jwt = jwt.unwrap();

        if !self.is_valid(jwt) {
            return false;
        }
        true
    }

    fn get_endpoint(&self, endpoint: &str) -> Result<String, Box<dyn std::error::Error>> {
        get_endpoint(&self.config.oidc_file, endpoint)
    }

    fn login(&self, user: &str, pass: &str) -> Result<String, Box<dyn std::error::Error>> {
        let http_response = send_login_to_endpoint(
            &self.get_endpoint("token").unwrap(),
            user,
            pass,
            &self.config.client_id,
            &self.config.client_secret,
        )?;
        let access_token = get_access_token(&http_response)?;
        Ok(access_token)
    }

    fn is_valid(&self, token: &str) -> bool {
        if token.is_empty() {
            return false;
        }
        if !token_time_is_valid(token).unwrap_or(false) {
            return false;
        }
        if !user_can_use_pam_oidc(token, self.config.client_id.as_str()).unwrap_or(false) {
            return false;
        }
        true
    }
}
