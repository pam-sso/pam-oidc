use log::error;
use pam::conv::PamConv;
use pam::module::PamHandle;

use crate::helpers::logging::set_logger;
use crate::oidc::Oidc;


fn get_conv(pamh: &PamHandle) -> Option<&PamConv> {
    match pamh.get_item::<PamConv>() {
        Ok(conv) => Some(conv),
        Err(err) => {
            error!("Error: couldn't get pam_conv: {:?}", err);
            None
        }
    }
}

pub fn prepare_pam_env(
    pamh: &PamHandle,
) -> Option<(&PamConv, Oidc)> {
    set_logger();
    let conv = match get_conv(pamh) {
        Some(conv) => conv,
        None => return None,
    };
    let oidc = match Oidc::new() {
        Some(oidc) => oidc,
        None => return None,
    };
    Some((conv, oidc))
}
