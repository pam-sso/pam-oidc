use fast_qr::qr::QRBuilder;

pub fn generate_qrcode(text: &str) -> String {
    QRBuilder::new(text)
        .build()
        .unwrap()
        .to_str()
}
