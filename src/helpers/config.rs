use log::{error, debug};
use std::fs::File;

use serde_derive::Deserialize;

static CONFIG_FILE: &str = "/etc/pam-oidc/config.json";

/// Config file :
/// 
/// {
///     "flow": "password|device",
///     "client_id": "TODO",
///     "client_secret": "TODO",
///     "oidc_file": "/etc/pam-oidc/openid-configuration.json"
/// }
#[derive(Deserialize)]
struct ConfigInput {
    flow: String,
    client_id: String,
    client_secret: String,
    oidc_file: String,
}

pub enum Flow {
    Password,
    Device,
}

pub struct Config {
    pub flow: Flow,
    pub client_id: String,
    pub client_secret: String,
    pub oidc_file: String,
}

pub fn get_config() -> Option<Config> {
    debug!("Reading config file");
    let f = File::open(CONFIG_FILE);
    if f.is_err() {
        error!("Cant read config file");
        return None;
    }
    let deserialized: Result<ConfigInput, serde_json::Error> = serde_json::from_reader(&f.unwrap());
    if deserialized.is_err() {
        error!("Cant deserialize config file");
        return None;
    }
    let data = deserialized.unwrap();
    return match data.flow.as_str() {
        "password" => Some(Config {
            flow: Flow::Password,
            client_id: data.client_id,
            client_secret: data.client_secret,
            oidc_file: data.oidc_file,
        }),
        "device" => Some(Config {
            flow: Flow::Device,
            client_id: data.client_id,
            client_secret: data.client_secret,
            oidc_file: data.oidc_file,
        }),
        _ => {
            error!("Unknown flow");
            None
        },
    }
}
