use log::LevelFilter;
use syslog::{BasicLogger, Facility, Formatter3164};

pub fn set_logger() {
    let formatter = Formatter3164 {
        facility: Facility::LOG_USER,
        hostname: None,
        process: "pam_oidc".into(),
        pid: 0,
    };

    let logger = syslog::unix(formatter).expect("could not connect to syslog");
    match log::set_boxed_logger(Box::new(BasicLogger::new(logger)))
        .map(|()| log::set_max_level(LevelFilter::Trace))
    {
        Ok(_) => (),
        Err(_err) => (),
    };
}
