#!/bin/bash

# reset database
docker compose down --volumes || true
docker compose up -d

# create admin user
docker compose exec keycloak /opt/jboss/keycloak/bin/add-user-keycloak.sh -u admin -p admin
docker compose restart keycloak

# create realm, users and clients
docker compose exec keycloak bash -c 'cat <<EOS | bash
while ! curl -fsL localhost:8080 -w "%{http_code}\n" -o/dev/null; do sleep 1; done
cd /opt/jboss/keycloak/bin/
./kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user admin --password admin
./kcadm.sh create realms -s realm=test -s enabled=true
./kcadm.sh create clients -r test -s secret=secret -f - <<EOF
{
  "id" : "337dae49-afc6-437f-8324-8c957338c532",
  "clientId" : "server1",
  "surrogateAuthRequired" : false,
  "enabled" : true,
  "alwaysDisplayInConsole" : false,
  "clientAuthenticatorType" : "client-secret",
  "redirectUris" : [ "http://localhost/" ],
  "webOrigins" : [ "http://localhost" ],
  "notBefore" : 0,
  "bearerOnly" : false,
  "consentRequired" : false,
  "standardFlowEnabled" : true,
  "implicitFlowEnabled" : false,
  "directAccessGrantsEnabled" : true,
  "serviceAccountsEnabled" : false,
  "publicClient" : false,
  "frontchannelLogout" : false,
  "protocol" : "openid-connect",
  "attributes" : {
    "id.token.as.detached.signature" : "false",
    "saml.assertion.signature" : "false",
    "saml.force.post.binding" : "false",
    "saml.multivalued.roles" : "false",
    "saml.encrypt" : "false",
    "oauth2.device.authorization.grant.enabled" : "true",
    "backchannel.logout.revoke.offline.tokens" : "false",
    "saml.server.signature" : "false",
    "saml.server.signature.keyinfo.ext" : "false",
    "use.refresh.tokens" : "true",
    "exclude.session.state.from.auth.response" : "false",
    "oidc.ciba.grant.enabled" : "false",
    "saml.artifact.binding" : "false",
    "backchannel.logout.session.required" : "false",
    "client_credentials.use_refresh_token" : "false",
    "saml_force_name_id_format" : "false",
    "require.pushed.authorization.requests" : "false",
    "saml.client.signature" : "false",
    "tls.client.certificate.bound.access.tokens" : "false",
    "saml.authnstatement" : "false",
    "display.on.consent.screen" : "false",
    "saml.onetimeuse.condition" : "false"
  },
  "authenticationFlowBindingOverrides" : { },
  "fullScopeAllowed" : true,
  "nodeReRegistrationTimeout" : -1,
  "defaultClientScopes" : [ "web-origins", "profile", "roles", "email" ],
  "optionalClientScopes" : [ "address", "phone", "offline_access", "microprofile-jwt" ],
  "access" : {
    "view" : true,
    "configure" : true,
    "manage" : true
  }
}
EOF
./kcadm.sh create users -r test -f - << EOF
{
  "username" : "user1",
  "enabled" : true,
  "credentials" : [
    {
      "type" : "password",
      "value" : "user1"
    }
  ]
}
EOF
./kcadm.sh create users -r test -f - << EOF
{
  "username" : "user2",
  "enabled" : true,
  "credentials" : [
    {
      "type" : "password",
      "value" : "user2"
    }
  ]
}
EOF
./kcadm.sh create roles -r test -s name=PAM
./kcadm.sh add-roles -r test --uusername user1 --rolename PAM
EOS'
