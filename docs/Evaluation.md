# Evaluation

This project has been made as a school project so yes we need to be evaluated...
You can find the report in the [report.md](report.md) file.

## Modalité d'evaluation

Les présentations se font en groupe : 10 groupes de 3.
Le temps de parole devra être équilibré :
environ 7 minutes par personne, ce qui donne :
10 présentations de 20 minutes suivies chacune de 10 minutes de questions.
La forme et l’organisation de la présentation sont  à l’appréciation de chaque groupe.

### Présentation orale

- 20 mn avec projection d’une série de diapositives (format ppt) comme support de la présentation
- 15 diapositives au maximum

*NB:* La copie des diapositives est remise au moment de la présentation.

### Synthèse écrite

Un rapport de synthèse du sujet traité :
15 pages minimum et 30 pages maximum comprenant :

- un résumé
- une introduction
- une conclusion
- une bibliographie
