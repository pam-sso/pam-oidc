#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <stdio.h>

const struct pam_conv conv = {
	misc_conv,
	NULL
};

int main(int argc, char *argv[]) {
	pam_handle_t* pamh = NULL;
	int retval;
	const char* user = "nobody";

	if(argc != 2) {
		printf("test.c> Usage: app [username]\n");
		exit(1);
	}

	user = argv[1];

	retval = pam_start("oidc-test", user, &conv, &pamh);

	retval = pam_authenticate(pamh, 0);
	if (retval == PAM_SUCCESS) {
		printf("test.c> auth ok\n");
	} else {
		printf("test.c> auth err\n");
		return 1;
	}

	retval = pam_acct_mgmt(pamh, 0);
	if (retval == PAM_SUCCESS) {
		printf("test.c> account ok\n");
	} else {
		printf("test.c> account err\n");
		return 1;
	}
	return 0;
}
