# PAM OIDC

This project provide some pam modules to authenticate users with OIDC.

To enable it you need to install the deb package form the release page.

Next you need to configure your pam module.

    auth sufficient pam_oidc.so /etc/pam-oidc/pam-oidc.json

Then download your oidc spec to the config folder:

    sudo curl -Ls -o /etc/pam-oidc/openid-configuration.json https://oidc.server/path/to/.well-known/openid-configuration

And finally add the client secrets to the `/etc/pam-oidc/pam-oidc.json` file:

    {
        "flow": "password",
        "client_id": "TODO",
        "client_secret": "TODO",
        "oidc_file": "/etc/pam-oidc/openid-configuration.json"
    }

## Flows

The flows can be configured in the `/etc/pam-oidc/pam-oidc.json` file (key `flow`).

Currently we only support the `password` flow but we are open to contributions (cf. [contributing](CONTRIBUTING.md)) to improve the software.

### Resource Owner Password

Basic user/password grant

## References and external ressources

- [Proof-of-Concept](https://gitlab.com/pam-sso/pam-oidc-old)
- [Interfacing crate](https://github.com/anowell/pam-rs/)
- [OIDC Documentation](https://developer.okta.com/docs/reference/api/oidc/)
